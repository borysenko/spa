import AllTask from './components/AllTask.vue';
import CreateTask from './components/CreateTask.vue';
import EditTask from './components/EditTask.vue';
import Login from './components/Login.vue';
import Register from './components/Register.vue';

export const routes = [
    {
        name:"login",
        path:"/login",
        component:Login,
        meta:{
            middleware:"guest",
            title:`Login`
        }
    },
    {
        name:"register",
        path:"/register",
        component:Register,
        meta:{
            middleware:"guest",
            title:`Register`
        }
    },
    {
        name: 'home',
        path: '/',
        component: AllTask
    },
    {
        name: 'create',
        path: '/create',
        component: CreateTask
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditTask
    }
];
