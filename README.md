Тестовое задание!

В файле .env прописать домен
```
APP_URL=http://spa.loc
SANCTUM_STATEFUL_DOMAINS=spa.loc

SESSION_DRIVER=cookie
```
выполнить в консоли
```
composer install
```
выполнить миграции
```
php artisan migrate
```
пометка: рикапча настроена на домены:
spa.loc,
localhost,
127.0.0.1

запустить в консоли
```
npm run dev
```
