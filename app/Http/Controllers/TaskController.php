<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all()->toArray();
        return array_reverse($tasks);
    }
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|max:255',
            'detail' => 'required',
        ]);

        $task = new Task([
            'name' => $request->input('name'),
            'detail' => $request->input('detail')
        ]);
        $task->save();
        return response()->json('Task created!');
    }
    public function show($id)
    {
        $task = Task::find($id);
        return response()->json($task);
    }
    public function update($id, Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|max:255',
            'detail' => 'required',
        ]);

        $task = Task::find($id);
        $task->update($request->all());
        return response()->json('Task updated!');
    }
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();
        return response()->json('Task deleted!');
    }
}
