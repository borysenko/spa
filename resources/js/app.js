import('./bootstrap');
import {createApp} from 'vue';
import App from './App.vue';
import VueAxios from 'vue-axios';
import Vuex from 'vuex';
import { createRouter, createWebHistory } from "vue-router";
import axios from 'axios';
import store from './store';
import { routes } from './routes';


const router = createRouter({
    history: createWebHistory(),
    routes: routes
});

window.router = router;

const app = createApp(App);
app.use(router);
app.use(Vuex);
app.use(store);
app.use(VueAxios, axios);
app.mount('#app');
